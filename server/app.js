require("dotenv").config();

const express = require("express");
const next = require("next");
const expressLayouts = require("express-ejs-layouts");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const flash = require("connect-flash");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const passport = require("passport");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const path = require("path");
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  // Passport config
  require("./config/passport")(passport);
  // DB Config
  const db = process.env.MongoURL;

  //Connect to Mongo
  mongoose
    .connect(
      db,
      { useNewUrlParser: true }
    )
    .then(() => console.log("MongoDB Connected!"))
    .catch(err => console.log(err));

  // EJS
  server.use(expressLayouts);
  server.set("view engine", "ejs");
  server.set("views", path.join(__dirname, "../server/views"));

  // Bodyparser
  server.use(bodyParser.urlencoded({ extended: true }));
  server.use(bodyParser.json());

  // Express session
  const store = new MongoDBStore({
    uri: db,
    collection: "mySessions"
  });
  store.on("error", function(error) {
    assert.ifError(error);
    assert.ok(false);
  });

  server.use(
    session({
      secret: "This is a secret cat",
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 // 1 year
      },
      store: store,
      // Boilerplate options, see:
      // * https://www.npmjs.com/package/express-session#resave
      // * https://www.npmjs.com/package/express-session#saveuninitialized
      resave: true,
      saveUninitialized: true
    })
  );

  //Passport middleware
  server.use(passport.initialize());
  server.use(passport.session());

  //Connect flash
  server.use(flash());

  // Global Vars
  server.use((req, res, next) => {
    res.locals.success_msg = req.flash("success_msg");
    res.locals.error_msg = req.flash("error_msg");
    res.locals.error = req.flash("error");
    next();
  });

  //Routes
  server.get("/", (req, res) => {
    req.isAuthenticated() ? res.redirect("/dashboard") : res.render("welcome");
  });

  server.use("/users", require("./routes/users"));

  //Dashboard
  server.get("/dashboard", (req, res) => {
    if (req.isAuthenticated()) {
      return handle(req, res);
    }
    req.flash("error_msg", "Please log in to view this resource");
    res.redirect("/users/login");
  });

  //Actual shit
  const User = require("./models/User");
  server.post("/add/:id", req => {
    User.findOneAndUpdate(
      { email: req.user.email },
      { $inc: req.body },
      { new: true },
      function(err) {
        if (err) {
          throw err;
        }
      }
    );
  });

  server.get("*", (req, res) => {
    return handle(req, res);
  });
  const PORT = process.env.PORT || 5000;
  server.listen(PORT, console.log(`Server started on port ${PORT}`));
});
