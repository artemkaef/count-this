const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  shitCounter: {
    type: Number,
    default: 0
  },
  fapCounter: {
    type: Number,
    default: 0
  },
  spaceX: {
    type: Number,
    default: 0
  }
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
