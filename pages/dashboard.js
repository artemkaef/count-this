import React from "react";
import Link from "next/link";
import Head from "next/head";
import Counter from "../components/Counter";
import styled from "styled-components";
import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
  html {
  background: #be93c5; /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #be93c5, #7bc6cc); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #be93c5, #7bc6cc); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  }
`;

export default class extends React.Component {
  static async getInitialProps({ req }) {
    const { name, spaceX, fapCounter, shitCounter } = req.user;
    return { name, spaceX, fapCounter, shitCounter };
  }
  render() {
    return (
      <>
        <GlobalStyle />
        <Container>
          <Head>
            <title>{this.props.name}'s Dashboard</title>
          </Head>
          <Title>
            Добро пожаловать <StyledName>{this.props.name}</StyledName>
          </Title>
        </Container>
        <CounterTitle>В 2019 году ты</CounterTitle>
        <CounterContainer>
          <Counter
            name="покекал"
            count={this.props.shitCounter}
            id="shitCounter"
            buttonValue="Я покекал!"
          />
          <Counter
            name="пофапал"
            count={this.props.fapCounter}
            id="fapCounter"
            buttonValue="Я пофапал!"
          />
          <Counter
            name="запустил SpaceX"
            count={this.props.spaceX}
            id="spaceX"
            buttonValue="Чё-то было!"
          />
        </CounterContainer>
        <Link href="/users/logout">
          <StyledButton>Тикать нахуй отсюда</StyledButton>
        </Link>
      </>
    );
  }
}

const StyledButton = styled.button`
  flex-shrink: 0;
  background-color: white;
  cursor: pointer;
  display: inline-block;
  color: palevioletred;
  font-size: 1.5rem;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  @media (max-width: 1000px) {
    font-size: 50px;
  }
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (max-width: 1000px) {
    margin-bottom: -30px;
  }
`;

const CounterContainer = styled.div`
  margin-top: 30px;
  font-size: 40px;
  margin-bottom: 170px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  @media (max-width: 1000px) {
    margin-top: 40px;
    font-size: 61px;
  }
`;
const Title = styled.h1`
  text-align: center;
  font-size: 44px;
  color: black;
  @media (max-width: 1000px) {
    font-size: 61px;
  }
`;
const CounterTitle = styled.h2`
  text-align: center;
  font-size: 35px;
  color: black;
  @media (max-width: 1000px) {
    font-size: 56px;
  }
`;

const StyledName = styled.span`
  letter-spacing: 1px;
  color: #85144b;
`;
