import React from "react";
import axios from "axios";
import styled from "styled-components";

class Counter extends React.Component {
  state = {
    count: this.props.count
  };

  declOfNum = (n, titles) => {
    return titles[
      n % 10 === 1 && n % 100 !== 11
        ? 0
        : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)
        ? 1
        : 2
    ];
  };
  handleClick = () => {
    this.setState({
      count: this.state.count + 1
    });

    axios.post(`/add/${this.props.id}`, { [this.props.id]: 1 });
  };
  render() {
    return (
      <Container>
        <div>
          {this.props.name}: <Count>{this.state.count}</Count>{" "}
          {this.declOfNum(this.state.count, ["раз", "раза", "раз"])}
        </div>
        <StyledButton onClick={this.handleClick}>
          {this.props.buttonValue}
        </StyledButton>
      </Container>
    );
  }
}

export default Counter;

const Container = styled.div`
  display: flex;
  margin-left: 50px;
  margin-right: 50px;
  margin-bottom: 30px;
  flex-wrap: wrap;
  flex-direction: column;
`;
const Count = styled.span`
  font-size: 45px;
  @media (max-width: 1000px) {
    font-size: 70px;
  }
`;
const StyledButton = styled.button`
  background-color: white;
  cursor: pointer;
  display: inline-block;
  width: auto;
  color: palevioletred;
  font-size: 1.5rem;
  margin: 1em;
  padding: 0.25em 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  @media (max-width: 1000px) {
    font-size: 50px;
  }
`;
